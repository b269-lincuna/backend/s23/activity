
// ############################ ACTIVITY 1  ############################

let trainer = {
    age: 10 ,
    friends: { 
        hoenn: ["May","Max"],
        kanto: ["Brock", "Misty"]
        },
    name: "Ash Ketchum",
    pokemons:  ["Pikachu","Charizard","Squirtle","Bulbasur"],
    talk: function (){
        console.log("Pikachu! I choose you!");
    }
}

console.log(trainer);

console.log("Result of dot notation: ");
console.log(trainer.name);


console.log("Result of square bracket notation: ");
console.log(trainer.pokemons);

console.log("Result of talk method: ");
trainer.talk();



// #######################################

// ############ GROUP ACTIVITY ###########



// Real World Application Of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack;
        console.log(target.name + " health is now reduced to " + target.health);
        
        if (target.health<=0){
                target.faint();
            }
        console.log(target);

        // if (target.health>=1 && target.health<=10){
        //         target.usePotion();
        //         console.log(target);
        //     }
    };

    this.faint = function(){
        console.log(this.name + ' fainted.');
    };

    // this.usePotion = function(){
    //     this.health += 20;
    //     console.log(this.name + " use potion and health is now increase to " + this.health);

    // };

}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewTwo = new Pokemon("MewTwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewTwo);

geodude.tackle(pikachu);
// geodude.tackle(pikachu);

mewTwo.tackle(geodude);
